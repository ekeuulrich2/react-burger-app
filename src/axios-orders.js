import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-burger-app-74524.firebaseio.com/'
});

export default instance;